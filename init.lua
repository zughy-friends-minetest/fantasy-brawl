fbrawl = {}
fbrawl.T = minetest.get_translator("fantasy_brawl")

local modpath = minetest.get_modpath("fantasy_brawl")


dofile(modpath .. "/src/SETTINGS.lua")


arena_lib.register_minigame("fantasy_brawl", {
  prefix = fbrawl_settings.prefix,
  icon = "fbrawl_icon.png",
  temp_properties = {
    classes = {}, -- pl_name: string = class: {}
    match_started = false,
    scores = {}   -- position: number = {pl_name = pl_name, kills = props.kills, score = score, id = score_id}
  },
  player_properties = {
    kills = 0,
    deaths = 0,
    ultimate_recharge = 0,
    hit_by = {}, -- {"player1" = <damage>, ...}
    is_invulnerable = false
  },
  hotbar = {
    slots = 1,
    background_image = "fbrawl_transparent.png",
    selected_image = "fbrawl_transparent.png"
  },
  hud_flags = {
    healthbar = false
  },
  disabled_damage_types = {"fall"},
  disable_inventory = true,
  load_time = fbrawl_settings.loading_time,
  show_nametags = false,
  show_minimap = false,
  celebration_time = fbrawl_settings.celebration_time,
  time_mode = "decremental",
  join_while_in_progress = true,
  can_drop = false,
})



dofile(modpath .. "/src/_deps/visible_wielditem.lua")

dofile(modpath .. "/src/_hud/hud.lua")
dofile(modpath .. "/src/sounds.lua")
dofile(modpath .. "/src/invulnerability.lua")

dofile(modpath .. "/src/_utils/generic.lua")
dofile(modpath .. "/src/_utils/vector.lua")
dofile(modpath .. "/src/_utils/movement.lua")
dofile(modpath .. "/src/_utils/arena_lib.lua")

dofile(modpath .. "/src/damage.lua")
dofile(modpath .. "/src/blood_effect.lua")
dofile(modpath .. "/src/health_bar.lua")

dofile(modpath .. "/src/classes/skill_layers/proxy_layer.lua")
dofile(modpath .. "/src/classes/skill_layers/ultimate_layer.lua")

dofile(modpath .. "/src/classes/skill_proxies/item_proxy.lua")
dofile(modpath .. "/src/classes/skill_proxies/aoe_proxy.lua")

dofile(modpath .. "/src/respawn/respawn.lua")
dofile(modpath .. "/src/controls.lua")

dofile(modpath .. "/src/classes/classes_system.lua")
dofile(modpath .. "/src/classes/skill_layers/meteors_layer.lua")
dofile(modpath .. "/src/classes/class_selector_formspec.lua")
dofile(modpath .. "/src/classes/hp_regen.lua")
dofile(modpath .. "/src/classes/book_pedestal.lua")

dofile(modpath .. "/src/_arena_lib/callbacks.lua")

dofile(modpath .. "/src/classes/warrior/warrior.lua")
dofile(modpath .. "/src/classes/mage/mage.lua")
dofile(modpath .. "/src/classes/infector/infector.lua")

--dofile(minetest.get_modpath("fantasy_brawl") .. "/src/_debug/debug_cmds.lua")
--dofile(minetest.get_modpath("fantasy_brawl") .. "/src/_debug/temp_entity.lua")

# Fantasy Brawl

The first brawler minigame on Minetest. Choose your class, prepare your skills and defeat them all!

## Assets License
All asset is CC0 unless it's in this exceptions list:
- all the files in the models folder by Giov4 -- License: Attribution 4.0
- fbrawl_puddle.ogg by mikaelfernstrom -- https://freesound.org/s/68729/ -- License: Attribution 4.0
- fbrawl_blob_explosion.ogg is a mix and tweak of:
	- very quick Splash and squishy sound by aarrnnoo -- https://freesound.org/s/516191/ -- License: Attribution 4.0
	- Long Splash and squishy sound by aarrnnoo -- https://freesound.org/s/516190/ -- License: Attribution 4.0
- fbrawl_rain.ogg by Arctura -- https://freesound.org/s/34069/ -- License: Attribution 3.0
- fbrawl_clock_ticking.ogg by joedeshon -- https://freesound.org/s/78563/ -- License: Attribution 4.0
- the ultimate textures and the class selection formspec textures are made by Zughy -- License: Attribution 4.0
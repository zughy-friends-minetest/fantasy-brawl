local S = fbrawl.T
local get_pl_by_name = minetest.get_player_by_name



local calculate_knockback = minetest.calculate_knockback
function minetest.calculate_knockback(player, hitter, time_from_last_punch, tool_capabilities, dir, distance, damage)
   local mod = arena_lib.get_mod_by_player(player:get_player_name())

   if mod == "fantasy_brawl" then return 0 end

   return calculate_knockback(player, hitter, time_from_last_punch,
   tool_capabilities, dir, distance, damage)
end



function fbrawl.hit_player(puncher, hit_pl, damage, knockback, self_damage)
	local puncher_name = puncher:get_player_name()
	local arena = arena_lib.get_arena_by_player(puncher_name)
	local hitting_themself = puncher_name == hit_pl:get_player_name()
	damage = damage * 20

	if
		 (hitting_themself and self_damage == false)
		 or arena.players[hit_pl:get_player_name()].is_invulnerable
	then
				return false
	end

	hit_pl:punch(puncher, 2, {damage_groups = {fleshy = damage}})

	if knockback then
		 hit_pl:add_velocity(knockback)
	end

	return true
end



function fbrawl.damage_players_near(puncher, pos, range, damage, knockback, callback)
	local arena = arena_lib.get_arena_by_player(puncher:get_player_name())

	if not arena then return false end

	if type(range) == "number" then range = vector.new(range, range, range) end

	for pl_name, props in pairs(arena.players) do
		 local hit_pl = get_pl_by_name(pl_name)
		 local hit_pl_pos = vector.add({x=0, y=1, z=0}, hit_pl:get_pos())

		 local is_close_enough = true
		 if
				math.abs(pos.x - hit_pl_pos.x) > range.x
				or math.abs(pos.y - hit_pl_pos.y) > range.y
				or math.abs(pos.z - hit_pl_pos.z) > range.z
		 then
				is_close_enough = false
		 end

		 if hit_pl == puncher then is_close_enough = false end

		 if is_close_enough then
				local did_it_hit = fbrawl.hit_player(puncher, hit_pl, damage, knockback)

				if did_it_hit and callback then
					 callback(hit_pl:get_player_name())
				end
		 end
	end
end



minetest.register_on_punchplayer(function(player, puncher, time_from_last_punch, tool_capabilities, dir, damage)
	local pl_name = player:get_player_name()
	local puncher_name = puncher:get_player_name()
	local arena = arena_lib.get_arena_by_player(pl_name)
	local mod = arena_lib.get_mod_by_player(player:get_player_name())

	if mod ~= "fantasy_brawl" or puncher_name == pl_name then return false end

	if fbrawl.is_player_playing(puncher_name) and not arena.players[pl_name].is_invulnerable then
		arena.players[pl_name].hit_by[puncher_name] = (arena.players[pl_name].hit_by[puncher_name] or 0) + damage
	end

	return false
end)
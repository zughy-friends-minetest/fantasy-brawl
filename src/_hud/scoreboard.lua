--[[
This file implements a dynamic scoreboard HUD that displays player rankings during matches.

Features:
- displays players ranked by kills, then by score (score = kills - deaths/2)
- handles ties correctly: tied players share the same position number and crown
- shows crowns for 1st (golden), 2nd (iron), and 3rd (bronze) positions
- dynamically adjusts vertical background height based on the number of top-3 positions
- highlights the current player's row
- shows kills/deaths for each player
- shows holding AUX1 key
]]

local function scoreboard_to_ranking(arena) end
local function get_crown_material(score_entry, arena) end
local function get_podium_count(arena) end
local function get_scoreboard_formspec(player_name) end

hud_fs.set_scale("fantasy_brawl:scoreboard", 80)
hud_fs.set_z_index("fantasy_brawl:scoreboard", 2000)
local active_scoreboards = {} -- [player_name] = true/nil



function fbrawl.update_arena_scores(arena)
  local scores = {}
  local score_id = 0

  -- calculate and store scores for each player
  for player_name, props in pairs(arena.players) do
    local score = props.kills - (props.deaths / 2)
    table.insert(scores, {player_name = player_name, kills = props.kills, score = score, id = score_id})
    score_id = score_id + 1
  end

  -- sort scores by kills, then score, then by id
  table.sort(scores, function(a, b)
    if a.kills ~= b.kills then
      return a.kills > b.kills
    elseif a.score ~= b.score then
      return a.score > b.score
    else
      return a.id > b.id
    end
  end)

  arena.scores = scores
end



-- get player ranks from scores:
-- returns a table where `rank[i]` is the rank of the player at index `i` in the scoreboard
-- if `i` and `i-1` have the same score, then `rank[i] == rank[i-1]`
function scoreboard_to_ranking(arena)
  local scores = arena.scores
  local rank = {}
  if #scores == 0 then
    return rank
  end

  for i, curr in ipairs(scores) do
    if i == 1 then
      rank[i] = 1
    else
      local prev = scores[i-1]
      if curr.kills == prev.kills and curr.score == prev.score then
        rank[i] = rank[i-1]
      else
        rank[i] = rank[i-1] + 1
      end
    end
  end

  return rank
end



-- determine the crown type based on rank
function get_crown_material(scoreboard_idx, arena)
  if not scoreboard_idx or #arena.scores == 0 then
    return nil
  end

  local entry_rank = scoreboard_to_ranking(arena)[scoreboard_idx]

  if entry_rank == 1 then return "golden" end
  if entry_rank == 2 then return "iron" end
  if entry_rank == 3 then return "bronze" end
  return nil
end



-- get the number of podium positions
function get_podium_count(arena)
  if #arena.scores == 0 then
    return 0
  end

  local rankings = scoreboard_to_ranking(arena)
  local count = 0
  for _, rank in pairs(rankings or {}) do
    if rank <= 3 then
      count = count + 1
    end
  end
  return count
end



-- generate the formspec for the scoreboard
function get_scoreboard_formspec(player_name)
  local arena = arena_lib.get_arena_by_player(player_name)
  if not arena then
    return ""
  end

  local podium_positions = get_podium_count(arena)
  local bg_height = (1.54 / 3) * podium_positions

  local formspec = string.format([[
    formspec_version[4]
    size[0.01,8.1]
    position[0.5,0.5]
    anchor[0.5,0.5]
    no_prepend[]
    bgcolor[#FF00FF00]

    style[small_label;border=false;font_size=*1;font=mono]
    style[title;border=false;font_size=*2;font=bold]
    style_type[label;font=mono]

    image[-4.85,0.8;0.6,%f;blank.png^[fill:1x1:#472d3ca9]
    image[-4.25,0;9.05,0.8;blank.png^[fill:1x1:#7a444a]
    image[3.05,0;0.8,0.8;fbrawl_warrior_icon.png]
    image[4.02,0.07;0.65,0.65;fbrawl_kills_icon.png^[transformFX]
    container[0,-0.45]
  ]], bg_height)

  local rankings = scoreboard_to_ranking(arena)

  -- add each player's data to the formspec
  for i, score_entry in ipairs(arena.scores) do
    local y_offset = (i - 1) * 0.5
    local is_current = (score_entry.player_name == player_name)
    local bg_color = is_current and "#472d3cf1" or "#472d3c66"
    local props = arena.players[score_entry.player_name]

		-- bg color and separating line
    formspec = formspec .. string.format([[
      container[0,%f]
      image[-4.25,1.25;9.05,0.5;blank.png^[fill:1x1:%s]
      image[-4.25,1.75;9.05,0.02;blank.png^[fill:1x1:#472d3c66]
    ]], y_offset, bg_color)

		-- crown icon
    local crown_type = get_crown_material(i, arena)
    if crown_type then
      formspec = formspec .. string.format(
        "image[-4.75,1.3;0.4,0.4;fbrawl_%s_crown.png]\n",
        crown_type
      )
    end

		-- place. name    kills   deaths
    formspec = formspec .. string.format([[
      label[-4,1.5;%d. %s]
      button[3,1;1,1;small_label;%d]
      button[3.85,1;1,1;small_label;%d]
      container_end[]
    ]], rankings[i], score_entry.player_name, props.kills, props.deaths)
  end

  formspec = formspec .. "container_end[]"
  return formspec
end



-- display the scoreboard HUD
function fbrawl.show_scoreboard(player_name)
  if active_scoreboards[player_name] then
    return
  end

  local player = minetest.get_player_by_name(player_name)
  if not player then
    return
  end

  active_scoreboards[player_name] = true
  local formspec = get_scoreboard_formspec(player_name)
  hud_fs.show_hud(player, "fantasy_brawl:scoreboard", formspec)
end



function fbrawl.close_scoreboard(player_name)
  local player = minetest.get_player_by_name(player_name)
  if not player then
    return
  end

  active_scoreboards[player_name] = nil
  hud_fs.close_hud(player, "fantasy_brawl:scoreboard")
end



-- register controls for showing/hiding the scoreboard

controls.register_on_press(function(player, control_name)
  local player_name = player:get_player_name()
  local mod = arena_lib.get_mod_by_player(player_name)
  local arena = arena_lib.get_arena_by_player(player_name)

  if mod == "fantasy_brawl" and arena.in_game and control_name == "aux1" then
    fbrawl.show_scoreboard(player_name)
  end
end)

controls.register_on_release(function(player, control_name)
  local player_name = player:get_player_name()
  local mod = arena_lib.get_mod_by_player(player_name)
  local arena = arena_lib.get_arena_by_player(player_name)

  if mod == "fantasy_brawl" and arena.in_game
     and not arena.in_celebration
     and control_name == "aux1" then
    active_scoreboards[player_name] = nil
    local p = minetest.get_player_by_name(player_name)
    if p then
      fbrawl.close_scoreboard(player_name)
    end
  end
end)



-- cleanup on player leave
minetest.register_on_leaveplayer(function(player)
  active_scoreboards[player:get_player_name()] = nil
end)

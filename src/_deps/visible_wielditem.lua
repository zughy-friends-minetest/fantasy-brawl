if core.get_modpath("visible_wielditem") then
	visible_wielditem.item_tweaks["groups"]["fbrawl_mesh"] = {
		rotation = {x=0, y=0, z=-110},
		position = {x=0, y=-0.35, z=0.25},
		scale = 1.25
	}

	visible_wielditem.item_tweaks["groups"]["fbrawl_big_mesh"] = {
		rotation = {x=0, y=0, z=-110},
		position = {x=0, y=-0.4, z=0.35},
		scale = 1.4
	}
end
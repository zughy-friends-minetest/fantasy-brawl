
ChatCmdBuilder.new("debug", function(cmd)
	cmd:sub("ultimate", function(name)
		local arena = arena_lib.get_arena_by_player(name)
		arena.players[name].ultimate_recharge = 5
	end)

	cmd:sub("add_pl :name :score", function(name, target_name, score)
		local arena = arena_lib.get_arena_by_player("Giov4")
		if not arena then return end

		score = tonumber(score)
		if not arena.players[target_name] then
			arena.players[target_name] = {
				kills = score,
				deaths = 0
			}
		else
			arena.players[target_name].kills = arena.players[target_name].kills + score
		end

		fbrawl.update_arena_scores(arena)
	end)

	cmd:sub("sounds", function(name)
		local id = -1
		local iteration = 0
		local play = function(self_ref)
			iteration = iteration + 1
			local c_id = id
			id = minetest.sound_play({name="fbrawl_puddle"}, {loop=true, to_player=name})
			minetest.sound_stop(c_id)
			if iteration > 20 then
				minetest.sound_stop(id)
				return
			end
			minetest.after(0.1, self_ref, self_ref)
		end
		play(play)
	end)
end)
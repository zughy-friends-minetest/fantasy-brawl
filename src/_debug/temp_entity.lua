minetest.register_entity("fantasy_brawl:t", {
	initial_properties = {
		visual = "cube",
		textures = {
			"fbrawl_wireframe.png","fbrawl_wireframe.png","fbrawl_wireframe.png",
			"fbrawl_wireframe.png","fbrawl_wireframe.png","fbrawl_wireframe.png"
		},
		use_texture_alpha = true,
		static_save = false,
		visual_size = {x = 64, y = 64}
	},
	timer = 0,
	on_step = function(self, dtime, moveresult)
		self.timer = self.timer + dtime
		if self.timer > 600 then
			self.object:remove()
		end
	end
})


function add_temp_entity(pos)
	minetest.add_entity(pos, "fantasy_brawl:t")
end

--[[
                     ! WARNING !
Don't change the variables names if you don't know what you're doing!

(fbrawl_settings.variable_name = value)
]]


-- ARENA LIB'S SETTINGS --

-- The table that stores all the global variables, don't touch this.
fbrawl_settings = {}

-- The maximum amount of time between the loading state and the start of the match.
fbrawl_settings.loading_time = 2

-- The time between the end of the match and the respawn at the hub.
fbrawl_settings.celebration_time = 8

-- What's going to appear in most of the lines printed by fantasy brawl.
fbrawl_settings.prefix = "Fantasy Brawl > "
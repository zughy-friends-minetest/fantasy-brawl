dofile(minetest.get_modpath("fantasy_brawl") .. "/src/respawn/respawn_hand.lua")



function fbrawl.respawn_player(pl_name)
	pl_name:start_skill("fbrawl:respawn")
end



skills.register_skill("fbrawl:respawn", {
	name = "Respawn",
	loop_params = {
		duration = 4,
		cast_rate = 1
	},
	data = {
		remaining_time = 0
	},

	blocks_other_skills = true,
	can_be_blocked_by_other_skills = false,
	stop_on_death = false,

	on_start = function(self)
		local arena = arena_lib.get_arena_by_player(self.pl_name)

		arena.players[self.pl_name].is_invulnerable = true
		self.player:get_inventory():set_list("main", {})
		self.player:set_hp(1) -- to avoid Luanti thinking the player is dead, so inputs work

		self.player:set_properties({
			visual_size = {x = 0, y = 0, z = 0},
			makes_footstep_sound = false,
			collisionbox = {-0.001, -0.001, -0.001, 0.001, 0.001, 0.001},
			pointable = false
		})

		fbrawl.reset_velocity(self.player)
		self.player:set_physics_override({
			speed = 0,
			jump = 0,
			gravity = 0,
			acceleration_default = 0,
			acceleration_air = 0
		})

		self.player:set_lighting({saturation = 0.35})

		if arena then arena_lib.teleport_onto_spawner(self.player, arena) end

		self.remaining_time = 4
	end,

	cast = function(self)
		arena_lib.HUD_send_msg(
			"title", self.pl_name,
			tostring(math.floor(self.remaining_time+0.5).."...")
		)

		self.remaining_time = self.remaining_time - 1
	end,

	on_stop = function(self)
		local arena = arena_lib.get_arena_by_player(self.pl_name)

		if arena and not arena.in_celebration then
			self.player:respawn()
			fbrawl.apply_class(self.pl_name, arena.classes[self.pl_name])
			arena.players[self.pl_name].is_invulnerable = false
		end

		arena_lib.HUD_hide("title", self.pl_name)


		self.player:set_properties({
			visual_size = {x = 1, y = 1, z = 1},
			makes_footstep_sound = true,
			collisionbox = {
					-0.30000001192093,
					0,
					-0.30000001192093,
					0.30000001192093,
					1.7000000476837,
					0.30000001192093,
			},
			pointable = true
		})

		self.player:set_lighting({saturation = 1})
	end
})



-- override death screen to not show anything
local old_death_screen = core.show_death_screen

core.show_death_screen = function(player, reason)
	local mod = arena_lib.get_mod_by_player(player:get_player_name())

	if mod == "fantasy_brawl" then
		return
	else
		old_death_screen(player, reason)
	end
end
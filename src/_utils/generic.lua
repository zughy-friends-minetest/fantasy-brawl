function fbrawl.replace_weapon(player, itemname, amount)
   local inv = player:get_inventory()
   local list = inv:get_list("main")
   local itemstack = ItemStack(itemname)
   itemstack:set_count(amount or 1)
   list[1] = itemstack

   inv:set_list("main", list)
end



function fbrawl.get_weapon(player)
   return player:get_inventory():get_list("main")[1]
end



-- min and max included, output format: 0.00
function fbrawl.random(min, max)
   min = min * 100
   max = max * 100
   return math.floor(math.random() * (max - min + 1) + min) / 100
end



function fbrawl.interpolate(a, b, factor)
   local distance = math.abs(a - b)
   local min_step = 0.1
   local step = distance * factor
   if step < min_step then step = min_step end

   if a > b then
      a = a - step
      if a <= b then
         a = b
      end
   else
      a = a + step
      if a >= b then
         a = b
      end
   end

   return a
end



function fbrawl.are_there_nodes_in_area(pos, range)
   range = range - 0.5
   local get_node = minetest.get_node
   local min_edge = vector.subtract(pos, range)
   local max_edge = vector.add(pos, range)
   local area = VoxelArea:new({MinEdge = min_edge, MaxEdge = max_edge})

   for i in area:iterp(min_edge, max_edge) do
      local pos = area:position(i)
      local node_name = get_node(pos).name

      if node_name ~= "ignore" and node_name ~= "air" then return true end
   end

   return false
end



function fbrawl.get_time_in_seconds()
   return minetest.get_us_time() / 1000000
end



function fbrawl.get_lowest_terrain_point(pos, range, grid_size, rays)
   local positions = fbrawl.grid_raycast_down(pos, range, grid_size, rays)
   if #positions > 0 then
      table.sort(positions, function(a, b)
         return a.above.y < b.above.y
      end)
      local pos = positions[1].above
      return {x = pos.x, y = pos.y - 0.5, z = pos.z}, true
   end
   return pos, false
end



function table.count(t)
   local count = 0
   for k, v in pairs(t) do
      count = count + 1
   end
   return count
end



function fbrawl.error(message)
   core.log("error", "[SKILLS] " .. message)
end

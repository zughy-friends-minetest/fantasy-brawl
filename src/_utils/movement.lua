local last_pls_y_velocity = {} -- pl_name = y_velocity
local get_node = minetest.get_node




function fbrawl.get_player_right_dir(player)
    local yaw = player:get_look_horizontal()
    local pl_right_dir = vector.new(math.cos(yaw), 0, math.sin(yaw))

    return vector.normalize(pl_right_dir)
end



function fbrawl.get_player_up_dir(pl)
    return vector.normalize(vector.rotate_around_axis(pl:get_look_dir(), fbrawl.get_player_right_dir(pl), math.pi / 2))
end



function fbrawl.is_on_the_ground(player)
    local pl_name = player:get_player_name()
    local under_pl_feet = player:get_pos()
    local pl_velocity = player:get_velocity()
    local last_y_velocity = last_pls_y_velocity[pl_name] or pl_velocity.y

    under_pl_feet.y = under_pl_feet.y - 0.4

    local is_on_the_ground =
        not (get_node(under_pl_feet).name == "air")
        or (pl_velocity.y == 0 and last_y_velocity < 0)

    last_pls_y_velocity[pl_name] = pl_velocity.y

    return is_on_the_ground
end



function fbrawl.reset_velocity(player)
    player:add_velocity(vector.multiply(player:get_velocity(), -1))
end

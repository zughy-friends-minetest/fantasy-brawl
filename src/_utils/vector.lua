function fbrawl.vec_interpolate(a, b, factor)
    local i = fbrawl.interpolate
    local f = factor
    local interpolated_vec = {x = i(a.x, b.x, f), y = i(a.y, b.y, f), z = i(a.z, b.z, f)}

    return interpolated_vec
end



function fbrawl.pl_look_at(player, target)
    local pos = player:get_pos()
    local delta = vector.subtract(target, pos)
    player:set_look_horizontal(math.atan2(delta.z, delta.x) - math.pi / 2)
end



function fbrawl.raycast(pos1, dir, range, entities)
    if entities == nil then entities = false end

    local pos2 = pos1 + vector.multiply(dir, range)
    local ray = minetest.raycast(pos1, pos2, entities, false)

    return ray
end



function fbrawl.look_raycast(object, range, entities)
    local pos = {}
    local looking_dir = 0

    -- Assigning the correct values to pos and looking_dir, based on
    -- if the object is a player or not.
    if object:is_player() then
        local pl_pos = object:get_pos()
        local head_pos = {x = pl_pos.x, y = pl_pos.y + 1.5, z = pl_pos.z}
        pos = head_pos
        looking_dir = object:get_look_dir()
    else
        pos = object:get_pos()
        looking_dir = vector.normalize(object:get_velocity())
    end

    -- Casts a ray from pos to the object looking direction * range.
    local ray = fbrawl.raycast(
        vector.add(pos, vector.divide(looking_dir, 4)),
        looking_dir,
        range,
        entities
    )

    return ray
end



function fbrawl.grid_raycast(player, range, radius, grid_size, entities)
    local pl = player
    local hit_pointed_things = {}
    local right_dir = fbrawl.get_player_right_dir(pl)
    local head_up_dir = fbrawl.get_player_up_dir(pl)
    local look_dir = pl:get_look_dir()
    local center = pl:get_pos() + look_dir + {x = 0, y = 1, z = 0}

    -- top-left corner of the player's camera
    local x_step = (radius * 2 / grid_size) * right_dir
    local y_step = (radius * 2 / grid_size) * head_up_dir
    local ray_pos = center - (x_step * (grid_size - 1)) / 2 + (y_step * (grid_size - 1)) / 2

    --draw_particles({image="fbrawl_gui_btn_choose.png", amount=5}, look_dir, ray_pos, 10, true)

    for row = 1, grid_size, 1 do
        for column = 1, grid_size, 1 do
            local pthings = fbrawl.from_ray_to_table(fbrawl.raycast(ray_pos, look_dir, range, entities))
            --draw_particles({image="fbrawl_gaia_fist.png", amount=5}, look_dir, ray_pos, 10, true)

            if pthings then
                table.insert_all(hit_pointed_things, pthings)
            end

            -- go to the next column
            ray_pos = ray_pos + x_step
        end

        -- go to the next row
        ray_pos = ray_pos - y_step
        ray_pos = ray_pos - x_step * grid_size
    end

    return hit_pointed_things
end



function fbrawl.grid_raycast_down(position, range, radius, grid_size, entities)
    local hit_pointed_things = {}
    local right_dir = {x = 1, y = 0, z = 0}
    local forward_dir = {x = 0, y = 0, z = 1}
    local down_dir = {x = 0, y = -1, z = 0}

    -- Calculate the size of each grid cell
    local cell_size = (radius * 2) / (grid_size - 1)

    -- Calculate the starting position (top-left corner of the grid)
    local start_pos = vector.subtract(position, vector.multiply(right_dir, radius))
    start_pos = vector.subtract(start_pos, vector.multiply(forward_dir, radius))

    for row = 1, grid_size do
        for col = 1, grid_size do
            -- Calculate the current ray starting position
            local ray_pos = vector.add(start_pos, vector.multiply(right_dir, (col - 1) * cell_size))
            ray_pos = vector.add(ray_pos, vector.multiply(forward_dir, (row - 1) * cell_size))

            -- Cast the ray downward
            local pthings = fbrawl.from_ray_to_table(fbrawl.raycast(ray_pos, down_dir, range, entities))

            if pthings then
                table.insert(hit_pointed_things, pthings[1])
            end
        end
    end

    return hit_pointed_things
end



function fbrawl.from_ray_to_table(ray)
    local output = {}

    if not ray then return {} end

    while true do
        local next_elem = ray:next()

        if not next_elem then
            return output
        elseif next_elem.type ~= "nothing" then
            table.insert(output, next_elem)
        end
    end
end



--[[
function draw_particles(particle, dir, origin, range, pierce)
    local check_coll = not pierce

    minetest.add_particlespawner({
        amount = particle.amount,
        time = 0.3,
        pos = vector.new(origin),
        vel = vector.multiply(dir, range),
        size = 2,
        collisiondetection = check_coll,
        collision_removal = check_coll,
        texture = particle.image
    })
end
--]]

fbrawl.min_kills_to_use_ultimate = 5



skills.register_layer("fbrawl:ulti_layer", {
	name = "Ultimate Template",
   cooldown = 2,  -- to track when it gets executed
	can_cast = function (self)
		local arena = arena_lib.get_arena_by_player(self.pl_name)
      return arena.players[self.pl_name].ultimate_recharge >= fbrawl.min_kills_to_use_ultimate
	end,
   cast = function(self)
      -- if no layer blocked this skill, then it was cast and I can deplete the recharge
      minetest.after(0, function ()
         if (self.active or self.cooldown_timer ~= 0) and self.player then
            local arena = arena_lib.get_arena_by_player(self.pl_name)
            local props = arena.players[self.pl_name]
            props.ultimate_recharge = 0
         end
      end)
   end
})
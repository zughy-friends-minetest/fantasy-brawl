local acid_puddle = {
   initial_properties = {
      hp_max = 999,
      physical = false,
      visual_size = {x = 1, y = 0.2},
      textures = {
         "fbrawl_acid_puddle_entity.png", "fbrawl_acid_puddle_entity.png",
         "fbrawl_transparent.png", "fbrawl_transparent.png",
         "fbrawl_transparent.png", "fbrawl_transparent.png"
      },
      initial_sprite_basepos = {x = 0, y = 0},
      pointable = false,
      visual = "cube",
      static_save = false,
   },
   damage = 4.5,
   time_accumulator = 0,
   has_to_shrink = false,
   pl_name = "",
   id = -1
}



function acid_puddle:on_activate(staticdata, dtime_s)
   local pl_name = minetest.deserialize(staticdata)[1]
   local obj = self.object
   local player = minetest.get_player_by_name(pl_name or "")

   if player then
      self.id = math.random(1, 100000)
      self.pl_name = pl_name
      self.max_size = minetest.deserialize(staticdata)[2]
      self.duration = minetest.deserialize(staticdata)[3]
      local slow_grow = minetest.deserialize(staticdata)[4] or false
      self.spread_speed = slow_grow and self.max_size*0.6 or self.max_size*2
      minetest.after(self.duration, function() self.has_to_shrink = true end)
   else
      obj:remove()
   end
end



function acid_puddle:on_step(dtime)
   local increase_size_per_step = self.spread_speed * dtime
   local obj = self.object
   local props = obj:get_properties()
   local player = minetest.get_player_by_name(self.pl_name)

   self.time_accumulator = self.time_accumulator + dtime

   if not player then
      obj:remove()
      return
   end

   if self.time_accumulator >= 0.25 then
      fbrawl.damage_players_near(player, obj:get_pos(), props.visual_size.x/2.5, 0, nil, function (pl_name)
         pl_name:unlock_skill("fbrawl:hit_by_acid_puddle")
         pl_name:get_skill("fbrawl:hit_by_acid_puddle"):stack(self.pl_name, self.id)
      end)

      if vector.distance(obj:get_pos(), player:get_pos()) < props.visual_size.x/2.5 then
         self.pl_name:unlock_skill("fbrawl:hit_by_acid_puddle")
         self.pl_name:get_skill("fbrawl:hit_by_acid_puddle"):stack(self.pl_name, self.id)
      end

      self.time_accumulator = self.time_accumulator - 0.2
   end

   if not self.has_to_shrink then
      props.visual_size = vector.add(props.visual_size, increase_size_per_step)
      props.visual_size.x = math.min(props.visual_size.x, self.max_size)
      props.visual_size.z = math.min(props.visual_size.z, self.max_size)
      props.visual_size.y = 0.2
      obj:set_properties(props)
   else
      props.visual_size = vector.subtract(props.visual_size, increase_size_per_step/3)
      props.visual_size.x = math.max(props.visual_size.x, 0)
      props.visual_size.z = math.max(props.visual_size.z, 0)
      props.visual_size.y = 0.2
      obj:set_properties(props)
   end

   if props.visual_size.x == 0 then
      obj:remove()
      return
   end
end



minetest.register_entity("fantasy_brawl:acid_puddle", acid_puddle)



--
-- SKILL SECTION
--

skills.register_skill("fbrawl:hit_by_acid_puddle", {
   loop_params = {
		duration = 0.3,
		cast_rate = 0.29
	},
   sounds = {
      bgm = {name = "fbrawl_puddle", to_player = true},
   },

   physics = {
      operation = "multiply",
      speed = 0.5
   },

   heal_moltiplicator = 0.50,
   damage_per_tick = 0.4,
   puddles = {},
   current_hitter = "",
   max_puddles_overlapping = 3,

   stack = function (self, hitter_name, puddle_id)
      -- calculate heal multiplier
      self.puddles[puddle_id] = true
      local puddles_count = math.min(table.count(self.puddles), self.max_puddles_overlapping)
      self.heal_moltiplicator = puddles_count * 0.50

      -- modifier counter hud text
      local counter_hud = self._hud and self._hud["counter"] or false
      if counter_hud then
         self.player:hud_change(counter_hud, "text", "x"..puddles_count)
      end

       -- activate skill
      if not self.is_active then
         if self.pl_name == hitter_name then
            self.physics = {
               operation = "multiply",
               speed = 1.3
            }
            self.hud = {
               {
                  name = "heal",
                  hud_elem_type = "image",
                  text = "fbrawl_healing_hud.png",
                  scale = {x=2.4, y=2.4},
                  position = {x=0.5, y=0.73-fbrawl.get_display_scale_offset(self.pl_name)},
               },
               {
                  name = "counter",
                  hud_elem_type = "text",
                  text = "x1",
                  number = 0xFFFFFF,
                  style = 1,
                  position = {x=0.5, y=0.73-fbrawl.get_display_scale_offset(self.pl_name)},
                  offset = {x=20, y=0}
               }
            }
         end
         self.current_hitter = hitter_name
         self.pl_name:start_skill("fbrawl:hit_by_acid_puddle", hitter_name)

      -- prolong skill if on a puddle of the same caster
      elseif self.current_hitter == hitter_name then
         if self._stop_job then self._stop_job:cancel() end
         self._stop_job = minetest.after(self.loop_params.duration, function()
            self:stop()
         end)
      end
   end,

   on_start = function (self, hitter_name)
      self.heal_moltiplicator = 0.50
      self.puddles = {}
   end,

   cast = function (self, hitter_name)
      local hitter = minetest.get_player_by_name(hitter_name)
      if not hitter then return false end
      fbrawl.hit_player(hitter, self.player, self.damage_per_tick, nil, false)
      if self.pl_name == hitter_name then
         self.player:set_hp(self.player:get_hp() + self.damage_per_tick * self.heal_moltiplicator * 20)
      end
   end,

   on_stop = function (self)
      self.physics = {
         operation = "multiply",
         speed = 0.7
      }
      self.hud = nil
   end
})
local S = fbrawl.T


skills.register_skill("fbrawl:acid_surf", {
	name = S("Acid Surf"),
	description = S("Surf on a wave of acid, damaging enemies in your path."),
	icon = "fbrawl_acid_surf_skill.png",
	sprint_force = 32,
	cooldown = 10,
	loop_params = {
		duration = 1.1,
		cast_rate = 0.1
	},
	sounds = {
		start = {name = "fbrawl_acid_surf"}
	},
	attachments = {
		particles = {{
			amount = 100,
			time = 0,
			minpos = {x = -0.6, y = 0, z = 1},
			maxpos = {x = 0.6, y = 0.5, z = 3},
			minvel = {x = -3, y = 1, z = 1},
			maxvel = {x = 3, y = 2.2, z = 1},
			minsize = 6,
			maxsize = 9,
			glow = 12,
			texture = {
				name = "fbrawl_poison_particle.png",
				alpha_tween = {1, 0.1},
				scale_tween = {
					{x = 1, y = 1},
					{x = 0.5, y = 0.5},
				},
				animation = {
					type = "vertical_frames",
					aspect_w = 16, aspect_h = 16,
					length = 0.5,
				},
			},
			minexptime = 0.3,
			maxexptime = 0.5,
		}},
	},
	on_start = function(self)
		local pl = self.player
		local velocity = vector.multiply(pl:get_look_dir(), self.sprint_force)
		velocity.y = 0

		pl:add_velocity(velocity)
	end,
	cast = function(self)
		local look_dir = self.player:get_look_dir() * 1.5
		look_dir.y = 0
		local spawn_pos, result = vector.add(self.player:get_pos(), look_dir)
		spawn_pos.y = spawn_pos.y + 1
		spawn_pos, result = fbrawl.get_lowest_terrain_point(spawn_pos, 2, 1, 2)

		if result then
			minetest.add_entity(spawn_pos, "fantasy_brawl:acid_puddle", minetest.serialize({self.pl_name, 2.5, 3}))
		end
	end
})

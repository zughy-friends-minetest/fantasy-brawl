local S = fbrawl.T



skills.register_skill_based_on("fbrawl:acid_spray", "fbrawl:blood_spray", {
	name = S("Blood Spray"),
	description = S("Spray blood to damage nearby enemies at the cost of your own health."),
	icon = "fbrawl_blood_spray.png",
	attachments = {
		hud = {{
			name = "blood",
			hud_elem_type = "image",
			text = "fbrawl_blood_spray.png",
			scale = {x=3, y=3},
			position = {x=0.5, y=0.82},
		}}
	},
	spray_particles = {
		texture = {
			name = "fbrawl_blood_particle_2.png",
			animation = "@@nil",
		},
		minsize = 3,
		maxsize = 5,
	},
	dmg_multiplier = 2,
	self_damage = 0.75,
	cast = function (self)
		fbrawl.hit_player(self.player, self.player, self.self_damage)
	end
})



controls.register_on_release(function(player, control_name)
	local pl_name = player:get_player_name()
	local blood_spray = pl_name:get_skill("fbrawl:blood_spray")

   if blood_spray and blood_spray.is_active and control_name == "RMB" then
      pl_name:stop_skill("fbrawl:blood_spray")
   end
end)

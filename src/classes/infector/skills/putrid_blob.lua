local S = fbrawl.T





--
-- SKILL
--

skills.register_skill("fbrawl:putrid_blob", {
	name = S("Putrid Blob"),
	description = S("Launches a putrid blob that explodes on impact, creating a toxic pool that damages enemies and heals you."),
	icon = "fbrawl_putrid_blob_skill.png",
	cooldown = 9,
	sounds = {
		cast = {name = "fbrawl_blob_spit"}
	},
	cast = function(self)
		local look_dir = self.player:get_look_dir()
		look_dir.y = 1.5
		local spawn_pos = vector.add(self.player:get_pos(), look_dir)
		minetest.add_entity(spawn_pos, "fantasy_brawl:putrid_blob", self.pl_name)
	end
})





--
-- ENTITY
--

local cs = 0.01 -- collision size
local blob = {
	initial_properties = {
		hp_max = 20,
		physical = true,
		collide_with_objects = true,
		collisionbox = {-cs, -cs, -cs, cs, cs, cs},
		visual = "mesh",
		mesh = "fbrawl_putrid_blob.obj",
		visual_size = {x = 60, y = 60, z = 60},
		textures = {"fbrawl_putrid_blob_entity.png"},
		pointable = false,
		static_save = false,
	},
	speed = 67.5,
	gravity = 45,
	damage = 4.5,
	pool_size = 12,
	pool_duration = 5,
	pl_name = "",
	has_to_collapse = false,
	trail_particles = -1
}



function blob:explode()
	local spread = {x=1, y=0, z=1}

	minetest.add_particlespawner({
		amount = 100,
		time = 0.2,
		collisiondetection = true,
		minvel = {x = -14, y = 5, z = -14},
		maxvel = {x = 14, y = 6, z = 14},
		minacc = {x=0, y=-27, z=0},
		maxacc = {x=0, y=-37, z=0},
		minsize = 4,
		maxsize = 6,
		texture = {
			name = "fbrawl_poison_particle.png",
			alpha_tween = {1, 0.1},
			scale_tween = {
				{x = 1, y = 1},
				{x = 0.5, y = 0.5},
			},
			animation = {
				type = "vertical_frames",
				aspect_w = 16, aspect_h = 16,
				length = 0.5,
			},
		},
		glow = 12,
		minexptime = 0.2,
		maxexptime = 0.6,
		minpos = vector.subtract(self.object:get_pos(), spread),
		maxpos = vector.add(self.object:get_pos(), spread),
	})

	minetest.sound_play({name = "fbrawl_blob_explosion"}, {pos = self.object:get_pos(), max_hear_distance = self.pool_size})
	minetest.sound_play({name = "fbrawl_blob_explosion"}, {to_player = self.pl_name})

	self.has_to_collapse = true
end



function blob:generate_trail_particles()
	self.trail_particles = minetest.add_particlespawner({
		amount = 100,
		time = 0,
		minpos = {x = -0.4, y = -0.4, z = -0.4},
		maxpos = {x = 0.4, y = 0.4, z = 0.4},
		minvel = {x = 0, y = -0.2, z = 0},
		maxvel = {x = 0, y = -0.1, z = 0},
		minsize = 4,
		maxsize = 6,
		minexptime = 0.2,
		maxexptime = 0.6,
		glow = 12,
		texture = {
			name = "fbrawl_poison_particle.png",
			alpha_tween = {1, 0.1},
			scale_tween = {
				{x = 1, y = 1},
				{x = 0.5, y = 0.5},
			},
			animation = {
				type = "vertical_frames",
				aspect_w = 16, aspect_h = 16,
				length = 0.5,
			},
		},
		attached = self.object
	})
end



function blob:on_activate(staticdata)
	if not staticdata or not minetest.get_player_by_name(staticdata) then
		self.object:remove()
		return
	end

	self:generate_trail_particles()

	-- SET PHYSICS
	self.pl_name = staticdata
	local player = minetest.get_player_by_name(staticdata)
	local yaw = player:get_look_horizontal()
	local pitch = player:get_look_vertical()
	local dir = player:get_look_dir()

	self.object:set_rotation({x = -pitch, y = yaw, z = 0})
	self.object:set_velocity({
		x = (dir.x * self.speed / 4.5),
		y = math.min((dir.y * self.speed), 20),
		z = (dir.z * self.speed / 4.5),
	})
	self.object:set_acceleration({x = dir.x * -4, y = -self.gravity, z = dir.z * -4})
end



function blob:on_step(dtime, moveresult)
	local collided_with_node = moveresult.collisions[1] and moveresult.collisions[1].type == "node"
	local collided_with_object = moveresult.collisions[1] and moveresult.collisions[1].type == "object"

	local player = minetest.get_player_by_name(self.pl_name)

	if not player then
		self.object:remove()
		return
	end

	-- COLLAPSING PHASE
	if self.has_to_collapse then
		local props = self.object:get_properties()
		local collapse_speed = 550
		props.visual_size.y = props.visual_size.y - collapse_speed * dtime
		props.visual_size.x = props.visual_size.x + (collapse_speed * 2.5) * dtime
		props.visual_size.z = props.visual_size.z + (collapse_speed * 2.5) * dtime

		-- COLLAPSE COMPLETE
		if props.visual_size.y <= 0 then
			self.object:remove()
			minetest.delete_particlespawner(self.trail_particles)
			return
		end

		self.object:set_properties(props)

	-- COLLISION PHASE
	elseif collided_with_node and moveresult.touching_ground then
		local spawn_pos = fbrawl.get_lowest_terrain_point(vector.add(self.object:get_pos(), vector.new(0,1,0)), 2, 0.5, 2)
		minetest.add_entity(spawn_pos, "fantasy_brawl:acid_puddle", minetest.serialize({self.pl_name, self.pool_size, self.pool_duration}))
		self:explode()

	-- COLLISION WITH PLAYER
	elseif collided_with_object and minetest.is_player(moveresult.collisions[1].object) then
		if moveresult.collisions[1].object:get_player_name() ~= self.pl_name then
			fbrawl.hit_player(player, moveresult.collisions[1].object, blob.damage)
		else
			local feet_pos = vector.subtract(player:get_pos(), 0)
			minetest.add_entity(feet_pos, "fantasy_brawl:acid_puddle", minetest.serialize({self.pl_name, self.pool_size, self.pool_duration}))
		end
		self:explode()

	end
end

minetest.register_entity("fantasy_brawl:putrid_blob", blob)
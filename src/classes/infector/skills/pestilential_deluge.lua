local function generate_cloud_particles(self) end
local function play_rain_sound(self) end

local S = fbrawl.T

local cloud_height = 15
local lifetime = 6
local size = 6
local clouds_grid = 2
local affected_area = size * clouds_grid*2

local rain_particles = {
	amount = 50,
	time = 0,
	minpos = vector.new(-size/2, -size/4, -size/2),
	maxpos = vector.new(size/2, size/4, size/2),
	minvel = {x = 0, y = -60, z = 0},
	maxvel = {x = 0, y = -55, z = 0},
	vertical = true,
	minsize = 3,
	maxsize = 6,
	texture = {
		name = "fbrawl_particle_acid_rain.png",
		alpha_tween = {0.5, 1},
	},
	maxexptime = 5,
	collisiondetection = true,
	collision_removal = true,
	object_collision = true,
}
local cloud_particle = {
	amount = 30,
	time = 0,
	minpos = vector.new(-size/2, -size/4, -size/2),
	maxpos = vector.new(size/2, size/4, size/2),
	minvel = {x = -0.5, y = -0.5, z = -0.5},
	maxvel = {x = 0.5, y = 0.5, z = 0.5},
	minsize = 55,
	maxsize = 60,
	texture = {
		name = "fbrawl_acid_cloud.png",
		alpha_tween = {1, 0},
		scale_tween = {
			{x = 0.9, y = 0.9},
			{x = 1,   y = 1},
		}
	},
	minexptime = 1,
	maxexptime = 1.5,
}





--
-- SKILL SECTION
--

skills.register_skill_based_on({"fbrawl:ulti_layer"}, "fbrawl:pestilential_deluge",	{
	name = S("Pestilential Deluge"),
	description = S("Summons clouds around the player that rain down acidic puddles, damaging any players caught in them and healing you."),
	icon = "fbrawl_pestilential_deluge_skill.png",
	sounds = {cast = {name = "fbrawl_thunder", max_hear_distance = affected_area}},
	cooldown = lifetime,
	cast = function (self)
		play_rain_sound(self)

		for x = -clouds_grid, clouds_grid do
			for z = -clouds_grid, clouds_grid do
				local pos = {x = self.player:get_pos().x + x*size, y = self.player:get_pos().y + cloud_height, z = self.player:get_pos().z + z*size}
				pos.x = pos.x + math.random(1, 300) / 100
				pos.z = pos.z + math.random(1, 300) / 100
				minetest.add_entity(pos, "fantasy_brawl:acid_cloud", self.pl_name)
			end
		end
	end
})





--
-- ENTITY SECTION
--

local cloud_def = {
	initial_properties = {
		hp_max = 20,
		physical = false,
		collide_with_objects = false,
		visual = "sprite",
		textures = {"fbrawl_transparent.png"},
		pointable = false,
		static_save = false,
	},
	pl_name = "",
	time_passed = 0,
	rain_particles = -1
}



function cloud_def:on_activate(staticdata)
	if not staticdata or not minetest.get_player_by_name(staticdata) then
		self.object:remove()
		return
	end
	self.pl_name = staticdata

	generate_cloud_particles(self)
end



function cloud_def:on_step(dtime)
	self.time_passed = self.time_passed + dtime

	if self.time_passed >= lifetime-0.5 then
		minetest.delete_particlespawner(self.rain_particles)
	end

	if self.time_passed >= lifetime then
		self.object:remove()
		return
 	end
end



minetest.register_entity("fantasy_brawl:acid_cloud", cloud_def)





--
-- LOCAL FUNCTIONS
--

function generate_cloud_particles(self)
	local cloud_particles = table.copy(cloud_particle)
	local rain_particles = table.copy(rain_particles)
	cloud_particles.attached = self.object
	rain_particles.attached = self.object

	minetest.add_particlespawner(cloud_particles)
	minetest.after(0.5, function ()
		if not minetest.get_player_by_name(self.pl_name) then return end

		self.rain_particles = minetest.add_particlespawner(rain_particles)

		local spawn_pos = fbrawl.get_lowest_terrain_point(self.object:get_pos(), cloud_height + 4, size, 4)
		minetest.add_entity(spawn_pos, "fantasy_brawl:acid_puddle", minetest.serialize({self.pl_name, size*2, lifetime, true}))
	end)
end



function play_rain_sound(self)
	local rain_sound = minetest.sound_play({name = "fbrawl_rain"}, {
		pos = self.player:get_pos(),
		max_hear_distance = affected_area,
		fade = 0.1
	})
	minetest.after(lifetime-1.5, function ()
		minetest.sound_fade(rain_sound, 0.1, 0)
	end)
end
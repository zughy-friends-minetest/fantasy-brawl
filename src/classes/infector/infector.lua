local S = fbrawl.T
local NS = function(s) return s end



fbrawl.register_class("Infector", {
   name = NS("Infector"),
   description = S("Do you want to shower them with poison? Make them drown an acid pool? Then, for you, there's nothing better than the Infector."),
   icon = "fbrawl_infector_icon.png",
   physics_override = {
      speed = 2.1,
   },
   weapon = "fantasy_brawl:acid_spray",
   skills = {
      LMB = "fbrawl:acid_spray",
      RMB = "fbrawl:blood_spray",
      SNEAK = "fbrawl:acid_surf",
      Q = "fbrawl:putrid_blob",
      ZOOM = "fbrawl:pestilential_deluge"
   },
   hp_regen_rate = 1
})


dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/shared_entities/acid_puddle.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/skills/acid_spray.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/skills/blood_spray.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/skills/acid_surf.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/skills/putrid_blob.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/skills/pestilential_deluge.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/infector/items.lua")
